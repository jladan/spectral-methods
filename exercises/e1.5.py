# Exercise 1.5: repeating p1 and p2 with different functions

import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt

from toclogger import TocLogger

from matplotlib import rc
rc('text', usetex=True)

# For various N, set up grid in [-pi,pi] and function u(x)
Nvec = 2**np.arange(3,13)
plt.figure()
# subplot('position', [.1, .4, .8, .5]) is done with axes() by pyplot
ax = plt.axes([.1, .55, .8, .4])

for N in Nvec:
    h = 2*np.pi/N
    x = -np.pi + np.arange(1,N+1)*h;
    # or x = linspace(-np.pi + h, np.pi, N)
    u = np.exp(np.sin(x))
    uprime = np.cos(x) * u
    v = np.exp(np.sin(x)**2)
    vprime = np.sin(2*x) * v
    w = np.exp(np.sin(x)**2)
    w[:N//2] = np.exp(-np.sin(x[:N//2])**2)
    wprime = np.sin(2*x) * w
    wprime[:N//2] = - wprime[:N//2]


    # construct sparse fourth-order diff. matrix
    e = np.ones(N)
    ii = np.arange(N)
    D = sp.coo_matrix((2*e/3, (ii, np.roll(ii, -1))), (N,N))
    D -= sp.coo_matrix((e/12, (ii, np.roll(ii, -2))), (N,N))
    D = (D - D.transpose())/h

    # plot max(abs(D*u - u'))
    error_u = np.linalg.norm(D.dot(u) - uprime, np.inf)
    error_v = np.linalg.norm(D.dot(v) - vprime, np.inf)
    error_w = np.linalg.norm(D.dot(w) - wprime, np.inf)
    plt.plot(N, error_u, 'o', color='b')
    plt.plot(N, error_v, 'o', color='g')
    plt.plot(N, error_w, 'o', color='r')

plt.plot(Nvec, Nvec**(-4.), linestyle='--')
plt.plot(Nvec, 10*Nvec**(-1.), linestyle='--')
ax.set_xscale('log')
ax.set_yscale('log')
ax.grid(True, which='both', axis='x')
ax.grid(True, which='major',axis='y')
ax.yaxis.set_ticks(10.**(np.array([0,-5,-10,-15])))

plt.xlabel('N')
plt.ylabel('error')
plt.title('Convergence of fourth-order finite differences')
ax.text(105, 5e-8, '$N^{-4}$', fontsize=18)
ax.text(105, 5e-4, '$N^{-1}$', fontsize=18)

# spectral convergence, now

from scipy.linalg import toeplitz

# For various N (even), set up grid in [-pi,pi] and function u(x)
Nvec = np.arange(2, 102, 2)
plt.figure()
# subplot('position', [.1, .4, .8, .5]) is done with axes() by pyplot
ax = plt.axes([.1, .4, .8, .5])

for N in Nvec:
    h = 2*np.pi/N
    x = -np.pi + np.arange(1,N+1)*h;
    u = np.exp(np.sin(x))
    uprime = np.cos(x) * u
    v = np.exp(np.sin(x)**2)
    vprime = np.sin(2*x) * v
    w = np.exp(np.sin(x)**2)
    w[:N//2] = np.exp(-np.sin(x[:N//2])**2)
    wprime = np.sin(2*x) * w
    wprime[:N//2] = - wprime[:N//2]

    # construct spectral differentiation matrix
    ii = np.arange(1, N)
    column = np.array([0, *(.5*(-1)**ii * np.cos(ii*h/2)/np.sin(ii*h/2))])
    D = toeplitz(column, column[[0, *range(N-1,0,-1)]])

    # plot max(abs(D*u - u'))
    error_u = np.linalg.norm(D.dot(u) - uprime, np.inf)
    error_v = np.linalg.norm(D.dot(v) - vprime, np.inf)
    error_w = np.linalg.norm(D.dot(w) - wprime, np.inf)
    plt.plot(N, error_u, 'o', color='b')
    plt.plot(N, error_v, 'o', color='g')
    plt.plot(N, error_w, 'o', color='r')

plt.plot(Nvec, 10*Nvec**(-1.), linestyle='--')
ax.set_xscale('log')
ax.set_yscale('log')
ax.grid(True, which='both', axis='x')
ax.grid(True, which='major',axis='y')
ax.yaxis.set_ticks(10.**(np.array([0,-5,-10,-15])))

plt.xlabel('N')
plt.ylabel('error')
plt.title('Convergence of spectral differentiation')
plt.show()


