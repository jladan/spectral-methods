# p3.py - band-limited interpolation

from pylab import *

h = 1; xmax = 10
x = arange(-xmax, xmax+h, h)
xx = arange(-xmax-h/20, xmax+h/20+h/10, h/10)

figure()
for plt in [1,2,3]:
    ax = subplot(3, 1, plt)
    if plt == 1:
        v = (x==0) * 1.
    elif plt == 2:
        v = (abs(x) <= 3) * 1.
    elif plt == 3:
        v = maximum(0, 1-abs(x)/3)
    plot(x, v, '.', markersize=8, color='k')
    p = zeros(shape(xx))
    for i in range(len(x)):
        p = p + v[i] * sin(pi*(xx-x[i])/h) / (pi*(xx-x[i])/h)
    ax.plot(xx,p,'',color='k')
    ax.set_ylim(-.5,1.5)
    ax.set_xticks([])
    ax.set_yticks([0, 1])
    ax.grid(True)

show()
