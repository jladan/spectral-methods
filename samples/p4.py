# p4.py - periodic spectral differentiation

from pylab import *
from scipy.linalg import toeplitz

# Set up grid and differentiation matrix
N=24
h = 2*pi/N
x = h*arange(1, N+1)
ii = np.arange(1, N)
column = np.array(np.insert(.5*(-1)**ii * np.cos(ii*h/2)/np.sin(ii*h/2), 0, 0))
D = toeplitz(column, column[np.insert(arange(N-1,0,-1), 0, 0)])
#column = np.array([0, *(.5*(-1)**ii * np.cos(ii*h/2)/np.sin(ii*h/2))])
#D = toeplitz(column, column[[0, *range(N-1,0,-1)]])

figure()
# Differentiation of hat function
v = maximum(0, 1-abs(x-pi)/2)
subplot(3,2,1)
plot(x,v,'.-')
axis([0, 2*pi, -.5, 1.5])
subplot(3,2,2)
plot(x, D.dot(v), '.-')
axis([0, 2*pi, -1, 1])

# Differentiation of exp(sin(x))
v = exp(sin(x))
vprime = cos(x)*v
subplot(3,2,3)
plot(x, v, '.-')
axis([0, 2*pi, 0, 3])
subplot(3,2,4)
plot(x, D.dot(v), '.-')
axis([0, 2*pi, -2, 2])
text(2.2, 1.4, "Max error = {}".format(max(abs(D.dot(v)-vprime))))


show()
