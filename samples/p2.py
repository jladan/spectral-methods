# p1.py - convergence of periodic spectral method (compare p1.py)

import numpy as np
from scipy.linalg import toeplitz
import matplotlib.pyplot as plt

from matplotlib import rc
rc('text', usetex=True)

# For various N (even), set up grid in [-pi,pi] and function u(x)
Nvec = np.arange(2, 102, 2)
plt.clf()
# subplot('position', [.1, .4, .8, .5]) is done with axes() by pyplot
ax = plt.axes([.1, .4, .8, .5])

for N in Nvec:
    h = 2*np.pi/N
    x = -np.pi + np.arange(1,N+1)*h;
    u = np.exp(np.sin(x))
    uprime = np.cos(x) * u

    # construct spectral differentiation matrix
    ii = np.arange(1, N)
    column = np.array([0, *(.5*(-1)**ii * np.cos(ii*h/2)/np.sin(ii*h/2))])
    D = toeplitz(column, column[[0, *range(N-1,0,-1)]])

    # plot max(abs(D*u - u'))
    error = np.linalg.norm(D.dot(u) - uprime, np.inf)
    plt.plot(N, error, 'o')

ax.set_xscale('log')
ax.set_yscale('log')
ax.grid(True, which='both', axis='x')
ax.grid(True, which='major',axis='y')
ax.yaxis.set_ticks(10.**(np.array([0,-5,-10,-15])))

plt.xlabel('N')
plt.ylabel('error')
plt.title('Convergence of spectral differentiation')

N = 8
h = 2*np.pi/N
x = -np.pi + np.arange(1,N+1)*h;
u = np.exp(np.sin(x))
uprime = np.cos(x) * u

plt.figure(2)
plt.plot(x, u)
plt.plot(x, uprime);
plt.figure(3)
plt.plot(x, np.exp(np.cos(x)))
plt.plot(x, -np.sin(x)*np.exp(np.cos(x)))

N = 10
h = 2*np.pi/N
x = -np.pi + np.arange(1,N+1)*h;
u = np.exp(np.sin(x))
uprime = np.cos(x) * u
plt.figure(2)
plt.plot(x, u)
plt.plot(x, uprime);
plt.figure(3)
plt.plot(x, np.exp(np.cos(x)))
plt.plot(x, -np.sin(x)*np.exp(np.cos(x)))
plt.show()


