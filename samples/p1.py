# p1.py - convergence of the fourth-order finite differences

import numpy as np
import scipy.sparse as sp
import matplotlib.pyplot as plt

from matplotlib import rc
rc('text', usetex=True)

# For various N, set up grid in [-pi,pi] and function u(x)
Nvec = 2**np.arange(3,13)
plt.clf()
# subplot('position', [.1, .4, .8, .5]) is done with axes() by pyplot
ax = plt.axes([.1, .4, .8, .5])

for N in Nvec:
    h = 2*np.pi/N
    x = -np.pi + np.arange(1,N+1)*h;
    # or x = linspace(-np.pi + h, np.pi, N)
    u = np.exp(np.sin(x))
    uprime = np.cos(x) * u

    # construct sparse fourth-order diff. matrix
    e = np.ones(N)
    ii = np.arange(N)
    D = sp.coo_matrix((2*e/3, (ii, np.roll(ii, -1))), (N,N))
    D -= sp.coo_matrix((e/12, (ii, np.roll(ii, -2))), (N,N))
    D = (D - D.transpose())/h

    # plot max(abs(D*u - u'))
    error = np.linalg.norm(D.dot(u) - uprime, np.inf)
    plt.plot(N, error, 'o')

plt.plot(Nvec, Nvec**(-4.), linestyle='--')
ax.set_xscale('log')
ax.set_yscale('log')
ax.grid(True, which='both', axis='x')
ax.grid(True, which='major',axis='y')
ax.yaxis.set_ticks(10.**(np.array([0,-5,-10,-15])))

plt.xlabel('N')
plt.ylabel('error')
plt.title('Convergence of fourth-order finite differences')
ax.text(105, 5e-8, '$N^{-4}$', fontsize=18)
plt.show()


